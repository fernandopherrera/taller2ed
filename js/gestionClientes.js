var clientesObtenidos;

function getClientes() {
  //alert("hola") //para checar si pasa por aquí en la ejecución.
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  //alert("hola") //para checar si pasa por aquí en la ejecución.
  request.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200) { //confirma que la petición terminó ok y petición correcta
      //console.table(JSON.parse(request.responseText).value);
      //alert("hola") //para checar si pasa por aquí en la ejecución.
      clientesObtenidos = request.responseText;
      procesarClientes();
      //alert("hola") //para checar si pasa por aquí en la ejecución.
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarClientes() {
  var JSONClientes = JSON.parse(clientesObtenidos);
  var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var divClientes		 = document.getElementById("divClientes");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONClientes.value.length; i++) {
    //console.log(JSONClientes.value[i].CostumerName);
    //alert("hola") //para checar si pasa por aquí en la ejecución.
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].ContactName;

    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;

    var columnaBandera = document.createElement("td");
    var imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");
    if (JSONClientes.value[i].Country != "UK") {
      imgBandera.src = rutaBandera + JSONClientes.value[i].Country + ".png";
    }else {
      imgBandera.src = rutaBandera + "United-Kingdom.png"
    }
    columnaBandera.appendChild(imgBandera);
    //columnaBandera.innerText = JSONClientes.value[i].UnitsInStock;
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divClientes.appendChild(tabla);
}
